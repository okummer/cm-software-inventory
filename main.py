import argparse
import json
import logging
import os
import subprocess
from datetime import date
from subprocess import PIPE


class Entry:
    source: str   # the collector that created this entry
    hostname: str  # the hostname of the machine this entry was collected from
    package: str  # the name of the package
    version: str  # the version of the package
    architecture: str
    description: str  # a short description of the package (optional)
    license: str  # whether the package is free to use or paid
    is_managed: bool  # is this package managed by the package manager?
    captured: str  # the date this entry was captured

    def __init__(self):
        self.captured = date.today().isoformat()

    def __str__(self):
        return f'{self.package} ({self.description}, version {self.version})'


def apt_description_collector(package_name: str):
    logging.debug('Getting description for package %s', package_name)
    out, err = subprocess.Popen(['apt', 'show', package_name],
                                stdout=subprocess.PIPE,
                                stderr=subprocess.PIPE).communicate()
    out = out.decode('utf-8').split('\n')
    for i in out:
        if i.startswith('Description:'):
            split = i.split('Description: ')
            return split[1] if len(split) > 1 else 'unknown'

    return 'unknown'


def apt_collector(hostname, **kwargs):
    logging.info('*** Collecting apt packages ***')
    out, err = subprocess.Popen(['apt', 'list', '--installed'], stdout=PIPE, stderr=PIPE).communicate()
    out = out.decode('utf-8').split('\n')[1:]
    result = []
    for i in out:
        entry = Entry()
        entry.source = 'apt'
        entry.hostname = hostname
        entry.package = i.split('/')[0]
        remainder = i.split(' ')
        entry.version = remainder[1] if len(remainder) > 1 else 'unknown'
        entry.architecture = remainder[2] if len(remainder) > 2 else 'unknown'
        entry.license = 'free'
        entry.description = apt_description_collector(entry.package) if not kwargs.get('skip_description') \
            else '** skipped **'
        entry.is_managed = True
        logging.info(f'{entry.source} is adding entry {entry} to inventory')
        result.append(entry)

    return result


def compgen_collector(hostname, **kwargs):
    logging.info('*** Collecting binaries in PATH via compgen ***')
    everything, err = subprocess.Popen(['bash', '-c', 'compgen -c | sort | uniq'],
                                       stdout=PIPE, stderr=PIPE).communicate()
    everything = everything.decode('utf-8').split('\n')
    builtins, err = subprocess.Popen(['bash', '-c', 'compgen -abfk | sort | uniq'],
                                     stdout=PIPE, stderr=PIPE).communicate()
    builtins = builtins.decode('utf-8').split('\n')
    # remove builtins from everything if existing
    for b in builtins:
        if b in everything:
            everything.remove(b)
    arch, err = subprocess.Popen(['uname', '-om'], stdout=PIPE, stderr=PIPE).communicate()
    arch = arch.decode('utf-8').split('\n')[0]

    result = []
    for program in everything:
        entry = Entry()
        entry.source = 'compgen'
        entry.hostname = hostname
        entry.package = program
        entry.version = 'unknown'
        entry.architecture = arch
        entry.license = 'free'
        entry.description = 'unknown'
        entry.is_managed = True
        logging.info(f'{entry.source} is adding entry {entry} to inventory')
        result.append(entry)
    return result


def npm_collector(hostname, **kwargs):
    logging.info('*** Collecting npm packages ***')
    npm_bin = os.getenv('NVM_BIN')
    out, err = subprocess.Popen([f'{npm_bin}/npm', 'list', '--parseable', '--long'],
                                stdout=PIPE, stderr=PIPE).communicate()
    out = out.decode('utf-8').split('\n')[1:-1]
    result = []
    for i in out:
        entry = Entry()
        entry.source = 'npm'
        entry.hostname = hostname
        line = i.split(':')
        remainder = line[1]
        package_splitpoint = 1 if remainder.startswith('@') else 0
        entry.package = remainder.split('@')[package_splitpoint]
        if package_splitpoint == 1:
            entry.package = f'@{entry.package}'
        entry.version = remainder.split('@')[package_splitpoint + 1].split(':')[0]
        entry.architecture = 'all'
        entry.license = 'free'
        entry.description = 'unknown'
        entry.is_managed = True
        logging.info(f'{entry.source} is adding entry {entry} to inventory')
        result.append(entry)

    return result


def json_emitter(hostname: str, results: list):
    with open(f'inventory-{hostname}.json', 'w') as f:
        for r in results:
            json.dump(r, f, default=vars)
            f.write('\n')


def generate_inventory(cmargs):
    result = []
    for collector in COLLECTORS:
        try:
            result += collector(cmargs.hostname, skip_description=cmargs.skip_description)
        except Exception as e:
            logging.error('Error while running collector %s: %s, skipping', collector.__name__, e)
    json_emitter(cmargs.hostname, result)


# main script starts here

COLLECTORS = [
    apt_collector,
    compgen_collector,
    npm_collector
]
parser = argparse.ArgumentParser(description='Generates an inventory of software packages installed on this machine')
parser.add_argument('hostname', type=str,
                    help='The hostname of your machine, typically something like ham-its-1234')
parser.add_argument('--loglevel', type=str, default='INFO', choices=['DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL'],
                    help='The log level to use')
parser.add_argument('--skip-description', action='store_true', help='Skip collecting package descriptions',
                    default=False)
args = parser.parse_args()
logging.basicConfig(format='%(levelname)s:%(message)s', level=args.loglevel)
generate_inventory(args)
